[Symbol_Game]: https://gitlab.com/ipog-mod/ipogclasses/-/raw/master/Sprites/Other/IJWLC0


# IPOG Mod Encyclopedia

## ![Symbol_Game]**Description**

It's the website for IPOG Mod, containing information about all of its parts. Feel free to use it and to contribute by any means!

The site is hosted on: https://ipog-mod.gitlab.io/


## ![Symbol_Game]Licensing

In Pursuit of Greed Encyclopedia project is licensed under the [GPL-3](http://www.gnu.org/licenses/gpl.html).

The data files (artwork, sound files, etc) are not covered in this license. The ones that came from In Pursuit of Greed have a [custom license](https://archive.org/details/GreedSource):

"These sources are provided for educational and historical purposes. No assets or code may be used in any way commercially. Personal and educational use only."
